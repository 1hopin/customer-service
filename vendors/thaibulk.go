package vendors

import (
	"fmt"
	"net/http"
	"os"

	"customer-service/constants"

	"customer-service/libraries"
)

func MessageRequest(data string) string {
	var url string = fmt.Sprintf("%s/sms", os.Getenv("THAIBULK_SMS_URL"))
	var headers [][]string
	headers = append(headers, []string{"Authorization", "Basic " + libraries.BasicAuth(os.Getenv("THAIBULK_MESSAGE_USER"), os.Getenv("THAIBULK_MESSAGE_PASS"))})
	statusCode, bodyBytes, err := libraries.RequestHttp(http.MethodPost, url, headers, constants.ApplicationForm, []byte(data), nil)

	if err != nil {
		fmt.Printf("Call Thaibulk Error >>>> %+v\n", err)
	}

	responseString := string(bodyBytes)
	if statusCode == http.StatusOK {
		fmt.Printf("Call to Thaibulk Success >>>> %+v\n", responseString)
	} else {
		fmt.Printf("Call to GolfGoGo Fail >>>> %+v\n", responseString)
	}

	return responseString
}

func OtpRequest(phone_no string) (int, string) {
	var url string = fmt.Sprintf("%s/otp/request", os.Getenv("THAIBULK_OTP_URL"))
	var headers [][]string

	data := fmt.Sprintf("msisdn=%s&secret=%s&key=%s", phone_no, os.Getenv("THAIBULK_OTP_SECRET"), os.Getenv("THAIBULK_OTP_KEY"))

	statusCode, bodyBytes, err := libraries.RequestHttp(http.MethodPost, url, headers, constants.ApplicationForm, []byte(data), nil)

	if err != nil {
		fmt.Printf("Call OTP Thaibulk Error >>>> %+v\n", err)
	}

	responseString := string(bodyBytes)
	if statusCode == http.StatusOK {
		fmt.Printf("Call to OTP Thaibulk Success >>>> %+v\n", responseString)
	} else {
		fmt.Printf("Call to OTP Thaibulk Fail >>>> %+v\n", responseString)
		//return 200, `{"status":"success","token":"KwPXBb2e396V8lvc2FKrGODaLdmA4ozj","refno":"GB7UW"}` //Mock Data
	}

	return statusCode, responseString
}

func OtpVerify(token, pin string) (int, string) {
	var url string = fmt.Sprintf("%s/otp/verify", os.Getenv("THAIBULK_OTP_URL"))
	var headers [][]string

	data := fmt.Sprintf("token=%s&pin=%s&secret=%s&key=%s", token, pin, os.Getenv("THAIBULK_OTP_SECRET"), os.Getenv("THAIBULK_OTP_KEY"))

	statusCode, bodyBytes, err := libraries.RequestHttp(http.MethodPost, url, headers, constants.ApplicationForm, []byte(data), nil)

	if err != nil {
		fmt.Printf("Call OTP Thaibulk Error >>>> %+v\n", err)
	}

	responseString := string(bodyBytes)
	if statusCode == http.StatusOK {
		fmt.Printf("Call to OTP Thaibulk Success >>>> %+v\n", responseString)
	} else {
		fmt.Printf("Call to OTP Thaibulk Fail >>>> %+v\n", responseString)
		//return 200, `{"status":"success","message":"Code is correct."}` //Mock Data
	}

	return statusCode, responseString
}
