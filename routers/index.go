package routers

import (
	"customer-service/controllers"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func SetupRouter(router *gin.Engine, db *gorm.DB) {
	customerCon := controllers.InitCustomerController()
	otpCon := controllers.InitOtpController()

	customerV1 := router.Group("/customer/v1/")
	{
		customerV1.POST("otps/request", otpCon.Request)
		customerV1.POST("otps/validate", otpCon.Verify)
		customerV1.POST("customers", customerCon.Register)
	}

	router.GET("/", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Customer Service is running.",
		})
	})
}
