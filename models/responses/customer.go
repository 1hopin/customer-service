package responses

type Customer struct {
	Id        int64  `json:"id"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	PhoneNo   string `json:"phone_no"`
}
