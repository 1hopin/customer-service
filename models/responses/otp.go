package responses

type OtpRequest struct {
	Status string `json:"status"`
	Token  string `json:"token"`
	Refno  string `json:"refno"`
}

type OtpVerify struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
