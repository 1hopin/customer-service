package requests

type OtpRequest struct {
	PhoneNo string `json:"phone_no" validate:"required"`
}

type OtpVerify struct {
	Token string `json:"token" validate:"required"`
	Pin   string `json:"pin" validate:"required"`
}
