package controllers

import (
	"customer-service/models/requests"
	"customer-service/models/responses"
	"customer-service/vendors"
	"encoding/json"
	"net/http"

	"customer-service/constants"

	"customer-service/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

type OtpController interface {
	Request(c *gin.Context)
	Verify(c *gin.Context)
}

type otpController struct {
}

func InitOtpController() OtpController {
	return &otpController{}
}

func (controller *otpController) Request(c *gin.Context) {
	var req requests.OtpRequest
	c.Bind(&req)

	if err := validator.New().Struct(req); err != nil {
		utils.RespondWithJSON(c, http.StatusBadRequest, constants.InvalidRequest, err.Error(), nil)
		return
	}

	// sender := "Demo"
	// message := fmt.Sprintf("Test Message")
	// data := fmt.Sprintf("msisdn=%s&message=%s&sender=%s", req.PhoneNo, message, sender)
	// vendors.MessageRequest(data)
	// For Send Message

	statusCode, responseString := vendors.OtpRequest(req.PhoneNo)

	if statusCode != http.StatusOK {
		utils.RespondWithJSON(c, http.StatusBadRequest, constants.Error, constants.Error, nil)
		return
	}

	var responseObj responses.OtpRequest
	json.Unmarshal([]byte(responseString), &responseObj)

	utils.RespondWithJSON(c, http.StatusOK, constants.Success, constants.Success, responseObj)
}

func (controller *otpController) Verify(c *gin.Context) {
	var req requests.OtpVerify
	c.Bind(&req)

	if err := validator.New().Struct(req); err != nil {
		utils.RespondWithJSON(c, http.StatusBadRequest, constants.InvalidRequest, err.Error(), nil)
		return
	}

	statusCode, responseString := vendors.OtpVerify(req.Token, req.Pin)

	var responseObj responses.OtpVerify
	json.Unmarshal([]byte(responseString), &responseObj)

	if statusCode != http.StatusOK {
		utils.RespondWithJSON(c, http.StatusBadRequest, constants.Error, constants.Error, nil)
		return
	}

	utils.RespondWithJSON(c, http.StatusOK, constants.Success, constants.Success, responseObj)
}
