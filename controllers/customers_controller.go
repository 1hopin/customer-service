package controllers

import (
	"customer-service/models/responses"
	"net/http"

	"customer-service/constants"

	"customer-service/utils"

	"github.com/gin-gonic/gin"
)

type CustomerController interface {
	Register(c *gin.Context)
}

type customerController struct {
}

func InitCustomerController() CustomerController {
	return &customerController{}
}

func (controller *customerController) Register(c *gin.Context) {
	customer := responses.Customer{
		Id:        1,
		Firstname: "Suttipong",
		Lastname:  "Ruanglitt",
		PhoneNo:   "0909861366",
	}
	utils.RespondWithJSON(c, http.StatusOK, constants.Success, constants.Success, customer)
}
