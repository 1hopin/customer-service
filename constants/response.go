package constants

const (
	Success        = "success"
	NotFound       = "not_found"
	IsExised       = "is_exised"
	InvalidRequest = "invalid_request"
	Invalid        = "invalid"
	Error          = "error"
	Unauthorized   = "unauthorized"
)
