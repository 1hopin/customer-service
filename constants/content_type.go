package constants

const (
	ApplicationJSON = "application/json; charset=utf-8"
	ApplicationForm = "application/x-www-form-urlencoded; charset=utf-8"
)
