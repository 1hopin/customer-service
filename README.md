# customer-service

# Before Start
1. Run Database repo http://git.barn364.com/1hopin/db
2. Open and Login http://localhost:5001/ (root:root)
3. create database "customer"
4. import file /db/init.sql or latest db init file from db repo
5. Copy File /.env_local to /.env

# Start Project
1. docker-compose up (--build for first time)
2. Run on terminal "curl --location --request GET 'localhost:8001'" to get healthcheck message
3. Enjoy!!